/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko2_1;

import java.util.Scanner;

/**
 *
 * @author Jesse Peltola
 */
public class Dog {

    private String name;

    public Dog(String name) {
        
        if(name.trim().isEmpty()) {
            this.name = "Huffi";
        } else {
            this.name = name;
        }
        
        System.out.println("Hei, nimeni on " + this.name);
        
    }
    //Speak metodi käynnistää lausahdus toiminnon.
    public void speak(String teksti) {
        
        Scanner scan = new Scanner(teksti);
        
        while (scan.hasNext()) {
            if (scan.hasNextInt()) {
                System.out.println("Such boolean: " + scan.hasNextInt());
            } else if (scan.hasNextBoolean()) {
                System.out.println("Such integer: " + scan.hasNextInt());
            } else {
                System.out.println(scan.hasNext());
            }
        }
         
        
    }
}
