/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko2_1;

import java.util.Scanner;

/**
 *
 * @author jesse
 */
public class Viikko2_1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner lukija = new Scanner(System.in);
        System.out.println("Anna koiralle nimi: ");
        String name1 = lukija.nextLine();
        Dog koira1 = new Dog(name1);
        System.out.println("Mitä koira sanoo: ");
        String lausahdus = lukija.nextLine();
        koira1.speak(lausahdus);
        
        
    }
    
}
