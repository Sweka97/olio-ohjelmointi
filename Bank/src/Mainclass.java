/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Scanner;

/**
 *
 * @author Jesse Peltola
 */
public class Mainclass {

    public static void main(String[] args) {
        int valinta = -1;
        Bank bank = new Bank() {
            };
        Scanner scan = new Scanner(System.in);
        while (valinta != 0) {
            System.out.println("\n*** PANKKIJÄRJESTELMÄ ***");
            System.out.println("1) Lisää tavallinen tili");
            System.out.println("2) Lisää luotollinen tili");
            System.out.println("3) Tallenna tilille rahaa");
            System.out.println("4) Nosta tililtä");
            System.out.println("5) Poista tili");
            System.out.println("6) Tulosta tili");
            System.out.println("7) Tulosta kaikki tilit");
            System.out.println("0) Lopeta");
            System.out.print("Valintasi: ");
            valinta = Integer.parseInt(scan.nextLine());

            
            switch (valinta) {

                case 1:

                    bank.addAcc();
                    break;
                case 2:
                    bank.addCreAcc();
                    break;
                case 3:
                    bank.deposit();
                    break;
                case 4:
                    bank.withdraw();
                    break;
                case 5:
                    bank.delAcc();
                    break;
                case 6:
                    bank.searchAccount();
                    break;
                case 7:
                    bank.printAll();
                    break;
                case 0:
                    valinta = 0;
                    break;
                default:
                    System.out.println("Valinta ei kelpaa.");
                    break;
            }
        }

        // TODO code application logic here
    }

}
