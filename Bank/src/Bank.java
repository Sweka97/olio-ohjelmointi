
import java.util.AbstractList;
import java.util.ArrayList;


import java.util.List;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jesse
 */
public class Bank {

    private List<Account> accounts = new ArrayList<Account>();
    private Scanner scan = new Scanner(System.in);

    public Bank() {

    }

    public void searchAccount() {

        System.out.print("Syötä tulostettava tilinumero: ");
        String accnum = scan.nextLine();

        for (Account a : accounts) {
            if (a.getAccNum().equals(accnum)) {
                System.out.println(a.toString());
            } else {
                System.out.println("Ei löydy.");
            }

        }

    }

    public void addAcc() {
        System.out.print("Syötä tilinumero: ");
        String tilinumero = scan.nextLine();

        System.out.print("Syötä rahamäärä: ");
        double maara = Integer.parseInt(scan.nextLine());

        CurAccount acc = new CurAccount(tilinumero, maara);
        accounts.add(acc);
        System.out.println("Tili luotu.");
    }

    public void addCreAcc() {
        System.out.print("Syötä tilinumero: ");
        String tilinumero = scan.nextLine();
        System.out.print("Syötä rahamäärä: ");
        double maara = Integer.parseInt(scan.nextLine());

        System.out.print("Syötä luottoraja: ");
        int luottoraja = Integer.parseInt(scan.nextLine());
        
        CreAccount creAcc = new CreAccount(tilinumero, maara, luottoraja);
        accounts.add(creAcc);
        System.out.println("Tili luotu.");
    }

    public void delAcc() {

        System.out.print("Syötä poistettava tilinumero: ");

        String accnum = scan.nextLine();
        List<Account> toRemove = new ArrayList<Account>();
        for (Account a: accounts) {
            if (a.getAccNum().equals(accnum)) {
                toRemove.add(a);
                System.out.println("Tili poistettu.");
            }
        }
        accounts.removeAll(toRemove);
        
        
    }

    public void withdraw() {
        System.out.print("Syötä tilinumero: ");
        String tilinumero = scan.nextLine();

        for (Account a : accounts) {
            if (a.getAccNum().equals(tilinumero)) {
                System.out.print("Syötä rahamäärä: ");
                double maara = Integer.parseInt(scan.nextLine());
                if (a.getAmount() >= maara) {
                    a.setAmount(a.getAmount() - maara);
                } else {
                    System.out.println("Et voi nostaa rahaa");
                }
            }
        }

    }

    public void deposit() {
        System.out.print("Syötä tilinumero: ");
        String tilinumero = scan.nextLine();

        for (Account a : accounts) {
            if (a.getAccNum().equals(tilinumero)) {
                System.out.print("Syötä rahamäärä: ");
                double maara = Integer.parseInt(scan.nextLine());
                if (maara > 0) {
                    a.setAmount(a.getAmount() + maara);
                }
            }
        }

    }

    public void printAll() {
        System.out.println("Kaikki tilit:");
        for (Account a : accounts) {
            System.out.println(a.toString());
        }

    }

}
