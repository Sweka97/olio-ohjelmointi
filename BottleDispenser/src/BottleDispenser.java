

import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jesse
 */
public class BottleDispenser {

    private int bottles;
    private float money;

    Bottle pull = new Bottle();
    



    
    private ArrayList<Bottle> automat = new ArrayList();

    public BottleDispenser() {
        bottles = 6;
        money = 0;

        for (int i = 0; i < bottles; i++) {
            Bottle pullo = new Bottle(i + 1);
            automat.add(pullo);

        }
    }

    public void addMoney() {
        money++;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }

    public void buyBottle(int index) {
        if (money >= automat.get(index).getPrice() && bottles > 0) {
            money -= automat.get(index).getPrice();
            bottles--;
            System.out.println("KACHUNK! " + automat.get(index).getName() + " tipahti masiinasta!");
            dispenser(index);
        } else if (money > pull.getPrice()) {
            System.out.println("Syötä rahaa ensin!");
        }

    }

    public void returnMoney() {
        
        //Rounding money to "#.##"
        float mo = (float) (Math.round(money * 100.0) / 100.0);
       
        //Converting float mo into String
        String mon = String.format("%.2f", mo);
        
        //Replacing "." to ","
        mon = mon.replace(".", ",");
        System.out.println("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + mon + "€");
        
        
    }

    public void tulostus() {

        for (int i = 0; i < automat.size(); i++) {
            System.out.println(i + 1 + ". Nimi: " + automat.get(i).getName() + "\n\t" + "Koko: " + automat.get(i).getSize() + "\t" + "Hinta: " + automat.get(i).getPrice());

        }

    }

    public void dispenser(int index) {
        automat.remove(index);

    }

}

