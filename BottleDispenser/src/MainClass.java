
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jesse 
 */
public class MainClass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner scan = new Scanner(System.in);
        BottleDispenser automaatti = new BottleDispenser();
        
        while(true) {
            System.out.println("\n*** LIMSA-AUTOMAATTI ***");
            
            System.out.println("1) Lisää rahaa koneeseen");
            System.out.println("2) Osta pullo");
            System.out.println("3) Ota rahat ulos");
            System.out.println("4) Listaa koneessa olevat pullot");
            System.out.println("0) Lopeta");
            
            
            //Pyydetään käyttäjältä 
            System.out.print("Valintasi: ");
            int valinta = Integer.parseInt(scan.nextLine()); //lukee linesta
           
            switch(valinta) {
                case 1:
                    automaatti.addMoney();
                    break;
                case 2:
                    
                    automaatti.tulostus();
                    System.out.print("Valintasi: ");
                    int val = Integer.parseInt(scan.nextLine());
                    automaatti.buyBottle(val-1);
                    break;
                case 3:
                    automaatti.returnMoney();
                    break;
                case 4:
                    automaatti.tulostus();
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Error");
            }
            
            if (valinta == 0) {
                break;
            }
        }
        
       
    }
    
}
