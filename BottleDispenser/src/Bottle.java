/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jesse
 */
public class Bottle {

    private String name;
    private String manufacturer;
    private double total_energy; //Tehtäväannossa pyydettiin 0.3 oletusarvoksi: int --> double
    private double price;
    private double size;

    //Pullon konstruktori
    public Bottle() {
        name = "Pepsi Max";
        manufacturer = "Pepsi";
        total_energy = 0.3;
        size = 0.5;
        price = 1.8;
    }

    public Bottle(String name, String manuf, double totE, double size, double price) {
        this.name = name;
        this.manufacturer = manuf;
        this.total_energy = totE;
        this.size = size;
        this.price = price;
    }

    public Bottle(int num) {
        switch (num) {
            case 1:
                name = "Pepsi Max";
                manufacturer = "Pepsi";
                total_energy = 0.3;
                size = 0.5;
                price = 1.8;
                break;
            case 2:
                name = "Pepsi Max";
                manufacturer = "Pepsi";
                total_energy = 0.3;
                size = 1.5;
                price = 2.2;
                break;
            case 3:
                name = "Coca-Cola Zero";
                manufacturer = "Coca-Cola";
                total_energy = 0.3;
                size = 0.5;
                price = 2;
                break;
            case 4:
                name = "Coca-Cola Zero";
                manufacturer = "Coca-Cola";
                total_energy = 0.3;
                size = 1.5;
                price = 2.5;
                break;
            case 5:
                name = "Fanta Zero";
                manufacturer = "Fanta";
                total_energy = 0.3;
                size = 0.5;
                price = 1.95;
                break;
            case 6:
                name = "Fanta Zero";
                manufacturer = "Fanta";
                total_energy = 0.3;
                size = 0.5;
                price = 1.95;
                break;
            default:
                break;
        }
    }

    public String getName() {
        return name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public double getEnergy() {
        return total_energy;
    }

    public double getPrice() {
        return price;
    }

    public double getSize() {
        return size;
    }
}
