    
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jesse
 */
public class Car {

    private wheel wh;
    private body bd;
    private chassis chas;
    private engine engi;

    private final ArrayList<String> lista = new ArrayList();
    
    public Car() {
        this.bd = new body();
        this.chas = new chassis();
        this.engi = new engine();
        for (int i = 0; i < 4; i++) {
            this.wh = new wheel();
        }
        lista.add("4 Wheel");
        
    }
    
  
    class wheel {
        
        public wheel() {
            System.out.println("Valmistetaan: Wheel");
            
        }
    }
    
    class body {
     
        public body() {
            System.out.println("Valmistetaan: Body");
            lista.add("Body");
        }
   
    }
    
    class chassis {
        
        public chassis() {
            System.out.println("Valmistetaan: Chassis");
            lista.add("Chassis");
        }
    }
    
    class engine {
        public engine() {
            System.out.println("Valmistetaan: Engine");
            lista.add("Engine");
        }
    }
    
    public void print() {
    
        System.out.println("Autoon kuuluu: ");
        for (int i = 0; i <= lista.size()-1; i++) {
            
            System.out.println("\t" + lista.get(i));
        }
    }

}
