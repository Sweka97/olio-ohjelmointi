
import java.io.IOException;


/**
 *
 * @author jesse
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        ReadAndWriteIO io = new ReadAndWriteIO();
        //io.readFile("input.txt");  
        //io.readAndWrite("input.txt", "output.txt");
        
        io.zipReader("zipinput.zip");
        
    }

}
