
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.zip.*;

/**
 *
 * @author jesse
 */
public class ReadAndWriteIO {

    //Week 4 exercise 1
    public void readFile(String s) {
        String output = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(s));

            while ((output = br.readLine()) != null) {

                System.out.println(output);

            }

            br.close();
        } catch (IOException ex) {
            System.out.println("Error");
        }
    }

    //Week 4 exercise 2
    public void readAndWrite(String in, String out) {
        String output;
        try {
            BufferedWriter wr;
            try (BufferedReader br = new BufferedReader(new FileReader(in))) {
                wr = new BufferedWriter(new FileWriter(out));
                while ((output = br.readLine()) != null) {
                    if (output.trim().length() > 0 && output.trim().length() < 30 && output.contains("v")) {
                        wr.write(output + "\n");
                    }

                }
            }
            wr.close();
        } catch (IOException ex) {
            System.out.println("Error");
        }
    }


    //Exercise 5
    public void zipReader(String in) throws IOException {
        ZipFile zipFile = new ZipFile(in);
        try (FileInputStream fis = new FileInputStream(in);
                BufferedInputStream bis = new BufferedInputStream(fis);
                ZipInputStream zis = new ZipInputStream(bis)) {

            ZipEntry entry;

            while ((entry = zis.getNextEntry()) != null) {
                InputStream input = zipFile.getInputStream(entry);

                try (BufferedReader br = new BufferedReader(new InputStreamReader(input, "UTF-8"))) {
                    String output;

                    while ((output = br.readLine()) != null) {
                        System.out.println(output);
                    }
                } catch (IOException e) {
                    System.out.println("Error");
                }

            }

        }

    }
}
